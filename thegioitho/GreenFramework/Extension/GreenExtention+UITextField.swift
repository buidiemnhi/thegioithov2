//
//  GreenExtention+UITextField.swift
//  Green
//
//  Created by Nhi Bui on 2/15/17.
//  Copyright © 2017 Nhi Bui. All rights reserved.TT
//

import UIKit
public extension UITextField
{
    public func setStyle(_ style : TextStyle)
    {
        self.font = greenDefine.getFontStyle(style).font
        self.textColor = greenDefine.getFontStyle(style).color
    }
    public func setBold()
    {
        self.font = UIFont(name: greenDefine.GreenFontBold.fontName, size: (self.font?.pointSize)!)
    }
    func setBottomBorder(withColor color: UIColor)
    {
        let border = CALayer()
        
        let width = CGFloat(1.0)
        
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.bounds.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true

    }
  

}
