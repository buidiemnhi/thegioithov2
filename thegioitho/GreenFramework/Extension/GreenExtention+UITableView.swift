//
//  GreenExtention+UITableView.swift
//  Green
//
//  Created by Nhi Bui on 2/15/17.
//  Copyright © 2017 Nhi Bui. All rights reserved.TT
//

import UIKit
public extension UITableView
{
    public func setIdentifier(_ identifier : String)
    {
        register(UINib.init(nibName: identifier , bundle: Bundle.main), forCellReuseIdentifier: identifier);
    }
    
    public func getIdentiferCell(_ identifier : String) -> UITableViewCell
    {
       return  dequeueReusableCell(withIdentifier: identifier)!
    }
    }
public extension UICollectionView {
    public func setIdentifier(_ identifier : String) {
        register(UINib.init(nibName: identifier , bundle: Bundle.main), forCellWithReuseIdentifier: identifier)
    }
    
    public func setIdentifier(_ identifier : String, kind: String) {
        register(UINib.init(nibName: identifier , bundle: Bundle.main), forSupplementaryViewOfKind: kind, withReuseIdentifier: identifier)
    }
}
