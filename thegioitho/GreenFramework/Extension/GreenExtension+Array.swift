//
//  GreenExtension+String.swift
//  Green
//
//  Created by Nhi Bui on 2/15/17.
//  Copyright © 2017 Nhi Bui. All rights reserved.TT
//

import UIKit
public extension Array
{

    mutating func rearrange(from: Int, to: Int) {
            insert(remove(at: from), at: to)
    }
    func indexOf<T : Equatable>(x:T) -> Int? {
        for var i in 0..<self.count {
            if self[i] as! T == x {
                return i
            }
        }
        return nil
    }
}
extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
    
}
extension Array {
    func commonElements<T: Sequence, U: Sequence>(_ lhs: T, _ rhs: U) -> [T.Iterator.Element]
        where T.Iterator.Element: Equatable, T.Iterator.Element == U.Iterator.Element {
            var common: [T.Iterator.Element] = []
            
            for lhsItem in lhs {
                for rhsItem in rhs {
                    if lhsItem == rhsItem {
                        common.append(lhsItem)
                    }
                }
            }
            return common
    }
}
