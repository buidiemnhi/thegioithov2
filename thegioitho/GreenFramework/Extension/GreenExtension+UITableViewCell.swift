//
//  GreenExtension+String.swift
//  Green
//
//  Created by Nhi Bui on 2/15/17.
//  Copyright © 2017 Nhi Bui. All rights reserved.TT
//

import UIKit

extension UITableViewCell {
    
    func hideSeparator() {
        self.separatorInset = UIEdgeInsets(top: 0, left: self.bounds.size.width, bottom: 0, right: 0)
    }
    
    func showSeparator() {
        self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
