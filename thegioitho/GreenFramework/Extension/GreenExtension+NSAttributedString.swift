//
//  GreenExtension+NSAttributedString.swift
//  N26
//
//  Created by Nhi Bui on 9/11/18.
//  Copyright © 2018 Nhi Bui. All rights reserved.
//

import Foundation
extension NSAttributedString {
    func rangeOf(string: String) -> Range<String.Index>? {
        return self.string.range(of: string)
    }
    
}
extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}

