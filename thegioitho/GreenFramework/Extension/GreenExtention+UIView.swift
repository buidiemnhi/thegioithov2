//
//  UIView+Extension.swift
//  LimeSoftware
//
//  Created by Nhi Bui on 10/11/16.
//  Copyright © 2016 Nhi Bui. All rights reserved.
//

import Foundation
import UIKit




let AnyViewTag = 101
//for button
typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

public enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint: CGPoint {
        return points.startPoint
    }
    
    var endPoint: CGPoint {
        return points.endPoint
    }
    
    var points: GradientPoints {
        switch self {
        case .topRightBottomLeft:
            return (CGPoint.init(x: 0.0, y: 1.0), CGPoint.init(x: 1.0, y: 0.0))
        case .topLeftBottomRight:
            return (CGPoint.init(x: 0.0, y: 0.0), CGPoint.init(x: 1, y: 1))
        case .horizontal:
            return (CGPoint.init(x: 0.0, y: 0.5), CGPoint.init(x: 1.0, y: 0.5))
        case .vertical:
            return (CGPoint.init(x: 0.0, y: 0.0), CGPoint.init(x: 0.0, y: 1.0))
        }
    }
}

extension UIView {
    
  }

public extension UIView
{
    func applyGradient(withColours colours: [UIColor], locations: [NSNumber]? = nil) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    func removeGradient()  {
        self.layer.sublayers?.remove(at: 0)
    }
   
    func addDashedBorder() {
        let color = UIColor.darkGray.cgColor
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    func applyGradient(withColours colours: [UIColor], gradientOrientation orientation: GradientOrientation) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
    func customActivityIndicator(view: UIView, widthView: CGFloat? = nil,backgroundColor: UIColor? = nil, message: String? = nil,colorMessage:UIColor? = nil ) -> UIView{
        
        //Config UIView
        self.backgroundColor = backgroundColor ?? UIColor.clear
        self.layer.cornerRadius = 10
        
        
        var selfWidth = view.frame.width - 100
        if widthView != nil{
            selfWidth = widthView ?? selfWidth
        }
        
        let selfHeigh = CGFloat(100)
        let selfFrameX = (view.frame.width / 2) - (selfWidth / 2)
        let selfFrameY = (view.frame.height / 2) - (selfHeigh / 2)
        let loopImages = UIImageView()
        
        //ConfigCustomLoading with secuence images
        let imageListArray = [UIImage(named:"indicator")]
        loopImages.animationImages = (imageListArray as! [UIImage])
        loopImages.animationDuration = TimeInterval(15)
        loopImages.startAnimating()
        let imageFrameX = (selfWidth / 2) - 17
        let imageFrameY = (selfHeigh / 2) - 35
        var imageWidth = CGFloat(100)
        var imageHeight = CGFloat(100)
        
        if widthView != nil{
            imageWidth = widthView ?? imageWidth
            imageHeight = widthView ?? imageHeight
        }
        
        //ConfigureLabel
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .gray
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.numberOfLines = 0
        label.text = message ?? ""
        label.textColor = colorMessage ?? UIColor.clear
        
        //Config frame of label
        let labelFrameX = (selfWidth / 2) - 100
        let labelFrameY = (selfHeigh / 2) - 10
        let labelWidth = CGFloat(200)
        let labelHeight = CGFloat(70)
        
        //add loading and label to customView
        self.addSubview(loopImages)
        self.addSubview(label)
        
        //Define frames
        //UIViewFrame
        self.frame = CGRect(x: selfFrameX, y: selfFrameY, width: selfWidth , height: selfHeigh)
        
        //ImageFrame
        loopImages.frame = CGRect(x: imageFrameX, y: imageFrameY, width: imageWidth, height: imageHeight)
        
        //LabelFrame
        label.frame = CGRect(x: labelFrameX, y: labelFrameY, width: labelWidth, height: labelHeight)
        
        return self
        
    }
    func copyView<T: UIView>() -> T
    {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
    
    public func addByView(_ view : UIView)
    {
        self.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        view.addSubview(self)
    }
    public func addBorder(edges : UIRectEdge, color : UIColor, alpha : CGFloat)
    {
        let borders = addBorder(edges, color: color)
        borders[0].alpha = alpha
    }

    public func addByViewWithlAutoSizing(_ view : UIView)
    {
        self.frame = CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height)
        view.addSubview(self)
        self.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight,UIViewAutoresizing.flexibleLeftMargin,UIViewAutoresizing.flexibleRightMargin,UIViewAutoresizing.flexibleTopMargin,UIViewAutoresizing.flexibleBottomMargin ]
    }
    
    public func removeSubsView()
    {
        for view in self.subviews
        {
            view.removeFromSuperview()
        }
    }

    func setY(y : CGFloat)
    {
        self.frame = CGRect.init(x: self.frame.origin.x, y: y, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    func getY() ->CGFloat
    {
        return self.frame.origin.y
    }
    func getHeight() ->CGFloat
    {
        return self.frame.size.height
    }
    
    func setHeight(height : CGFloat)
    {
        self.frame.size.height = height
    }

    public func drawRound()
    {
        self.layer.cornerRadius = self.frame.size.width/2;
        self.clipsToBounds = true;
    }
    
    public func drawRadius(_ radius  : CGFloat)
    {
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 0.5
        self.layer.masksToBounds = true
        self.clipsToBounds = true;

        self.layer.borderColor = UIColor.clear.cgColor
    }
    
    public func drawRadius(_ radius  : CGFloat,color : UIColor , thickness :CGFloat )  {
        self.layer.cornerRadius = radius
        self.layer.borderWidth = thickness
        self.layer.masksToBounds = true
        self.layer.borderColor = color.cgColor
    }
    func dropShadow(color: UIColor) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 6
    }
    func dropShadowInsec()
    {
        self.layer.masksToBounds = false;
        self.layer.shadowOffset = CGSize.init(width: 1, height: 1)
        self.layer.shadowRadius = 4;
        self.layer.shadowOpacity = 0.12;
        self.layer.shadowColor = UIColor.black.cgColor
    }
    public func addBorder(_ edges: UIRectEdge, color: UIColor, thickness: CGFloat = 0.5) -> [UIView]{
        var borders = [UIView]()
        func border() -> UIView {
            let border = UIView(frame: CGRect.zero)
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            return border
        }
        
        if edges.contains(.top) || edges.contains(.all) {
            let top = border()
            addSubview(top)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[top(==thickness)]",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["top": top]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[top]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["top": top]))
            borders.append(top)
        }
        
        if edges.contains(.left) || edges.contains(.all) {
            let left = border()
            addSubview(left)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[left(==thickness)]",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["left": left]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[left]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["left": left]))
            borders.append(left)
        }
        
        if edges.contains(.right) || edges.contains(.all) {
            let right = border()
            addSubview(right)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:[right(==thickness)]-(0)-|",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["right": right]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[right]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["right": right]))
            borders.append(right)
        }
        
        if edges.contains(.bottom) || edges.contains(.all) {
            let bottom = border()
            addSubview(bottom)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:[bottom(==thickness)]-(0)-|",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["bottom": bottom]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[bottom]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["bottom": bottom]))
            borders.append(bottom)
        }
        
        return borders
        
    }
    
    
    func setBottomBorder(withColor color: UIColor, withthick thick :CGFloat )
    {
        let border = CALayer()
        
        let width = thick
        
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.bounds.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }
    
    func verticalView(views : [UIView])
    {
        var targtIndex : Int = -1 ;
        for   i in  0..<views.count
        {
            let limeUnit  : UIView = views[i]
            limeUnit.frame  = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
            self.addSubview(limeUnit)
            var previousXView : UIView!
            var targetView : UIView!
            
            if(i > 0)
            {
                previousXView   = views[i-1]
            }
            
            if(targtIndex != -1 )
            {
                targetView = views [targtIndex]
            }
            limeUnit.translatesAutoresizingMaskIntoConstraints = false
            self.addConstraint(NSLayoutConstraint(item: limeUnit, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0))
            
            self.addConstraint(NSLayoutConstraint(item: limeUnit, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0))
            if(i == 0 )
            {
                self.addConstraint(NSLayoutConstraint(item: limeUnit, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 0))
            }
            else if (i == views.count - 1)
            {
                self.addConstraint(NSLayoutConstraint(item: limeUnit, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0))
                
                let verticalSpacing1 = NSLayoutConstraint(item: previousXView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: limeUnit, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
                NSLayoutConstraint.activate([verticalSpacing1])
            }
                
            else
            {
                let verticalSpacing1 = NSLayoutConstraint(item: previousXView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: limeUnit, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0)
                NSLayoutConstraint.activate([verticalSpacing1])
            }
            if(views.count == 1)
            {
                let widthContrain  = NSLayoutConstraint(item: limeUnit, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0)
                NSLayoutConstraint.activate([widthContrain])
            }
            
            if(targtIndex != -1 )
            {
                let widthContrain  = NSLayoutConstraint(item: limeUnit, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: targetView, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0)
                NSLayoutConstraint.activate([widthContrain])
                
            }
            if(targtIndex == -1 )
            {
                targtIndex = i ;
            }
        }
    }
    
    
    func setLayout(_ view : UIView) // full layout for sub view
    {
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 0))
    }
    
    
    
    
}
