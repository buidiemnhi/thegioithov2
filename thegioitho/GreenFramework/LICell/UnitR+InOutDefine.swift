//
//  UnitR+Public.swift
//  LiCell
//
//  Created by KieuVan on 10/27/16.
//  Copyright © 2016 KieuVan. All rights reserved.
//

import UIKit
//import Limerence




extension UnitR {
    
    class func initTile(title : String) -> UnitR
    {
        return  UnitR.initTile(title: title, agliment: .center,style : TextStyle.h2General);
    }
    
    
    class func initContent(content : String) -> UnitR
    {
        return  UnitR.initContent(text: content, aligment: .right, color: UIColor.black , font: UIFont.systemFont(ofSize: 12.0)
        )
    }
    
    class func initContent(content : String,aligment : NSTextAlignment) -> UnitR
    {
        return  UnitR.initContent(text: content, aligment: aligment, color: UIColor.black , font: UIFont.systemFont(ofSize: 12.0)
        )
    }

    
    class func initContent(content : String, color : UIColor) -> UnitR
    {
        return  UnitR.initContent(text: content, aligment: .right, color: color , font: UIFont.systemFont(ofSize: 12.0)
        )
    }
    class func initContent(content : String, color : UIColor, aligment : NSTextAlignment) -> UnitR
    {
        return  UnitR.initContent(text: content, aligment: aligment, color: color , font: UIFont.systemFont(ofSize: 12.0)
        )
    }


    
    
    class func initContent(text : String, aligment : NSTextAlignment, color : UIColor ,font : UIFont) -> UnitR
    {
        let label = UILabel.init()
        label.text = text;
        label.textAlignment = aligment
        label.textColor = color
        label.font = font
        let unitR : UnitR = UnitR.init()
        unitR.addSubview(label)
        unitR.setLayout(label)
        
        unitR.backgroundColor = UIColor.clear;
        label.backgroundColor = UIColor.clear;
        return  unitR;

    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    class func initCustomView(view : UIView) -> UnitR
    {
        let unitR : UnitR = UnitR.init()
        unitR.addSubview(view)
        unitR.setLayout(view)
        return unitR
    }

    
    
    class func initTile(title : String,agliment : NSTextAlignment,style : TextStyle) -> UnitR
    {
        let label = UILabel.init()
        label.text = title;
        label.setStyle(style)
        label.textAlignment = agliment
        let unitR : UnitR = UnitR.init()
        unitR.addSubview(label)
        unitR.setLayout(label)
        
        unitR.backgroundColor = UIColor.clear;
        label.backgroundColor = UIColor.clear;
        return  unitR;
    }
    
    class func initField(title : String,agliment : NSTextAlignment) -> UnitR
    {
        let textField = UITextField.init()
        textField.text = title;
        textField.textAlignment = agliment
        let unitR : UnitR = UnitR.init()
        unitR.addSubview(textField)
        unitR.setLayout(textField)
        return  unitR;
    }

    func fillData(newUnit : UnitR)
    {
    }
}
