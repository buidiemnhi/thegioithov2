//  KCell.swift
//  LiCell
//  Created by KieuVan on 10/25/16.
//  Copyright © 2016 KieuVan. All rights reserved.

import UIKit
@objc class CCell: UICollectionViewCell {
    var space : NSMutableArray = NSMutableArray()
    private    var anies : [Any]!
    private  var store : [Any]!
    var isDraw : Bool = false;
    var isHeader : Bool = false
    var identifier : String = ""
    var indexPath : IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initStyle()
    }
    
    func initStyle()
    {
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initStyle()
    }
    
    func drawSpace(count : Int)
    {
        for i  in 0..<count
        {
            let spaceUnit : SpaceUnit = SpaceUnit()
            if(isHeader)
            {
                spaceUnit.registerHeader(name: identifier.appending(String(i)))
            }
            else
            {
                spaceUnit.registerContent(name: identifier.appending(String(i)))
            }
            spaceUnit.calulatorWidth(unitRs: anies[i] as! [UnitR]);
            space.add(spaceUnit)
        }
        
        self.contentView.verticalView(views: space as! [UIView])
    }
    
    func setHeaderData(anies : [Any], identifier : String)
    {
        isHeader = true
        self.identifier = identifier ;
        setHeaderData(anies: anies)
    }
    
    func setHeaderData(anies : [Any])
    {
        isHeader = true
        setData(anies: anies)
    }
    
    func setData(anies :[Any])
    {
        self.anies = anies
        if(!self.isDraw)
        {
            self.isDraw = true
            drawData()
            headerStyle()
        }
        else
        {
            fetchData()
        }
    }
    
    func headerStyle()  {
        
        if(isHeader)
        {
            self.contentView.backgroundColor = UIColor.white ;
        }
    }
    
    func fetchData()
    {
        for i in 0..<anies.count
        {
            let unitRs = anies[i] as! [UnitR]
            let spaceUnit : SpaceUnit = space[i] as! SpaceUnit
            spaceUnit.update(units: unitRs)
        }
    }
    
    func drawData()
    {
        drawSpace(count: anies.count)
        for i in 0..<anies.count
        {
            let unitRs = anies[i] as! [UnitR]
            let spaceUnit : SpaceUnit = space[i] as! SpaceUnit
            spaceUnit.drawLayer(units: unitRs)
            spaceUnit.update(units: unitRs)
            spaceUnit.postSync()
        }
    }
    
}

