


import UIKit
enum DrawType
{
    case vertical
    case horizen
}

class UnitR: SpaceUnit
{

    var title : String = ""
    var drawType : DrawType = .vertical
    var storeLocal : NSMutableArray = NSMutableArray()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    init(anies : [Any])
    {
        super.init(frame: CGRect.init())
        draw(anies: anies, drawType: drawType)
    }

    init(anies : [Any] , drawType : DrawType)
    {
        super.init(frame: CGRect.init())
        draw(anies: anies, drawType: drawType)
    }

    func draw(anies : [Any] , drawType : DrawType )
    {
        self.drawType = drawType
        storeLocal = NSMutableArray.init(array: anies)
        drawView(drawType: drawType)
    }
    
    func drawView(drawType : DrawType)
    {
        switch drawType {
        case .vertical:
            verticalView(views: storeLocal as! [UIView])
            break
        case .horizen :
           // horizental(unitRs: storeLocal)
            break
        }
    }

    func drawCornerImage()
    {
        if(isHeader)
        {
            let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 2, height: 2))
            self.addSubview(imageView)
            imageView.backgroundColor = UIColor.red;
        }
    }
}
