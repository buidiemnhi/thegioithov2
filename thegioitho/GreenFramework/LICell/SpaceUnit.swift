//
//  DrawUnit.swift
//  MTS
//
//  Created by KieuVan on 3/3/17.
//  Copyright © 2017 InnoTech. All rights reserved.
//

import UIKit
class SpaceUnit: UIView {
    
    var layers : [UIView] = [UIView]()
    var widt : Float = 0
    var identifier : String = ""
    var index : Int = 0
    var target : Int = 0
    let sync : String = "_Sync"
    let syncProcess : String = "_SyncProcess"
    var isLine : Bool = true ;

    var isHeader : Bool = false ;
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    
    func drawLayer(units : [UnitR])
    {
        for _ in 0..<units.count
        {
            let view : UIView = UIView.init()
            self.addSubview(view)
            self.setLayout(view)
            layers.append(view);
        }
        targetData(newValue: 0)
        
        if(isHeader)
        {
            if(units.count > 1)
            {
                drawTrigger()
            }
        }
    }
    
    func update(units : [UnitR])
    {
        for i in 0..<units.count
        {
            let view : UIView = layers[i]
            view.removeSubsView()
            view.addSubview(units[i])
            view.setLayout(units[i])
        }

    }
    
    
    func registerHeader(name : String)
    {
        isHeader = true
        self.identifier = name;
        let tap : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(SpaceUnit.push))
        self.addGestureRecognizer(tap)

        NotificationCenter.default.addObserver(self, selector: #selector(SpaceUnit.nextData), name: NSNotification.Name(rawValue: name), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SpaceUnit.postResponseSync), name: NSNotification.Name(rawValue:self.identifier.appending(sync)), object: nil)
    }
    
    func drawTrigger()
    {
        
        let imageView : UIImageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 8, height: 8))
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.image = "TriggerLayer".imageTint(UIColor.white)
        
        

        let widthConstraint = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 8)
        let heightConstraint = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 8)
        
        let trailing = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0)
        
        let top = NSLayoutConstraint(item: imageView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
        
        imageView.addConstraints([widthConstraint, heightConstraint])
        self.addConstraints([trailing,top])
        imageView.updateConstraints()
        self.updateConstraints()

    }
    
    
    
    
    
    func registerContent(name : String)
    {
        self.identifier = name;
        NotificationCenter.default.addObserver(self, selector: #selector(SpaceUnit.nextData), name: NSNotification.Name(rawValue: name), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(synProcessF), name: NSNotification.Name(rawValue:self.identifier.appending(syncProcess)), object: nil)

    }
    
    @objc func nextData()
    {
        if(index >= layers.count)
        {
            index = 0
        }
        targetData(newValue: index)

    }
    
    func targetData(newValue : Int)
    {
        self.target = newValue
        index = newValue + 1
        for i in 0..<layers.count
        {
            let view : UIView = layers[i]
            view.isHidden = true;
        }
        
        if(layers.count >  newValue)
        {
            layers[newValue].isHidden = false;
        }

    }



    @objc func push()
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:identifier), object: nil)
    }

    func postSync()
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:identifier.appending(sync)), object: nil)
    }


    @objc func synProcessF(notifier : NSNotification)
    {
        let requesIndex  : Int  = (notifier.object as! NSNumber).intValue
        self.targetData(newValue: requesIndex)
        
    }

    @objc func postResponseSync()
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:identifier.appending(syncProcess)), object: NSNumber.init(value: self.target));
        
    }


    func calulatorWidth(unitRs : [UnitR])
    {
        var maxWidth  : Float = 0 ;
        for   i in  0..<unitRs.count
        {
            let limeUnit  : UnitR = unitRs[i]
            if limeUnit.widt  > maxWidth
            {
                maxWidth = limeUnit.widt
            }
        }
        self.widt = Float(maxWidth)
    }


}
