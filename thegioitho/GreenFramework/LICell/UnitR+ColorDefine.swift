//
//  UnitRColorDefine.swift
//  LiCell
//
//  Created by KieuVan on 10/27/16.
//  Copyright © 2016 KieuVan. All rights reserved.
//

import UIKit


let UnitRColor = UnitRColorDefine.sharedInstance()

class UnitRColorDefine: NSObject
{
    static var instance: UnitRColorDefine!
    class func sharedInstance() -> UnitRColorDefine
    {
        self.instance = (self.instance ?? UnitRColorDefine())
        return self.instance
    }
    
    
    
    
    
    var headerBackgroundColor : UIColor!
    var headerTextColor : UIColor = UIColor.white
    var headerFont : UIFont = UIFont.systemFont(ofSize: 12)
    
    
    var headerSeparatorColor : UIColor!
    var rowBackgroundColor : UIColor!
    var rowTextColor : UIColor!
    var rowSeparatorColor : UIColor!
    
}
