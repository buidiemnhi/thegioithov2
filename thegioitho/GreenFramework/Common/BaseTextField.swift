//
//  BaseTextField.swift
//  TheGioiTho
//
//  Created by HaiComet on 8/7/18.
//  Copyright © 2018 HaiComet. All rights reserved.
//

import UIKit
import SnapKit

class BaseTextField: UITextField {
    var isShowLeft = false
    var isShowRight = true
    
    
    
    @IBInspectable
    var placeHolderColor: UIColor = UIColor.clear{
        didSet {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: placeHolderColor])
        }
    }
    @IBInspectable
    var setEnable : Bool = true{
        didSet{
            self.isEnabled = setEnable
            if(setEnable != true ){
                self.inputView = UIView.init()
            }
        }
    }

    @IBInspectable
    var radius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = radius
        }
    }
    
    
    @IBInspectable
    var borderWith: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWith
        }
    }
    
    @IBInspectable
    var leftTransform:CGFloat = 0.0 {
        didSet {
            self.layer.sublayerTransform = CATransform3DMakeTranslation(leftTransform, 0, 0)
        }
    }
    @IBInspectable
    var rightTransform:CGFloat = 0.0 {
        didSet {
            self.layer.sublayerTransform = CATransform3DMakeTranslation(0, rightTransform, 0)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    override func prepareForInterfaceBuilder() {
        setup()
    }
    func setup(){self.backgroundColor = UIColor.white
        
    }
    
   
    @IBInspectable var icTextField: String? {
       
        didSet{
            
          let containerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width:30, height: self.frame.height))
            let imageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = UIImage.init(named: icTextField!)
             imageView.contentMode = .scaleAspectFit
           containerView.addSubview(imageView)
            self.rightView = containerView
            imageView.center = containerView.center
            //self.rightView = containerView
            self.rightViewMode = UITextFieldViewMode.always
        }
    }
    @IBInspectable var LabelText :String? {
        didSet{
            
            let containerView: UIView = UIView(frame: CGRect(x: 10.0, y: 0, width:70, height: self.frame.height))
            let label = UILabel.init(frame: CGRect(x:  10.0, y: 0, width: 70, height: self.frame.height))
            label.text = LabelText
            label.font = UIFont.systemFont(ofSize: 12.0)
           
            containerView.addSubview(label)
            self.leftView = containerView
            label.center = containerView.center
            //self.rightView = containerView
            self.leftViewMode = UITextFieldViewMode.always
        }
    }

    
}
