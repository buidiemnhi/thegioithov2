//
//  LocationManager.swift
//  TheGioiTho
//
//  Created by HaiComet on 8/7/18.
//  Copyright © 2018 HaiComet. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationManager()
    
    fileprivate var locationManager: CLLocationManager!
    var currentLocation: CLLocation! {
        didSet {
            guard (self.locationListen) != nil else {
                return
            }
            self.locationListen!(currentLocation)
        }
    }
    
    override init() {
        super.init()
        if CLLocationManager.locationServicesEnabled() == false {
            print("Please enable location services")
        }
        
        if CLLocationManager.authorizationStatus() == .denied {
            print("Please authorize location services")
        }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.distanceFilter = 5.0
        locationManager.startUpdatingLocation()
    }
    
    var locationListen: ((_ location: CLLocation) -> Void)?
    
    func getCurrentLocation(done: @escaping (_ location: CLLocation?) -> Void) {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            self.locationManager.requestWhenInUseAuthorization()
        }
        else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            print("Location Update Ok")
        }
        else {
            print("Location Update Denied")
            done(nil)
        }
        if currentLocation != nil {
            done(currentLocation)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if currentLocation == nil {
            currentLocation = locations.last
        }
        else{
            if currentLocation != nil && currentLocation.coordinate.latitude != locations.last?.coordinate.latitude &&  currentLocation.coordinate.longitude != locations.last?.coordinate.longitude{
                currentLocation = locations.last
            }
        }
    }
    
    private func locationManager(manager: CLLocationManager, didFailWithError error: Error) {
        if CLLocationManager.authorizationStatus() == .denied {
            print("Please authorize location services")
        }
    }
    
}
