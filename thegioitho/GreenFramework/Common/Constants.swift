//
//  Constants.swift
//  TheGioiTho
//
//  Created by HaiComet on 8/7/18.
//  Copyright © 2018 HaiComet. All rights reserved.
//

import UIKit

struct FontName {
    
    static let THEME_FONT_SF_DISPLAY_BLACK         = "SFUIDisplay-Black"
    static let THEME_FONT_SF_DISPLAY_BOLD          = "SFUIDisplay-Bold"
    static let THEME_FONT_SF_DISPLAY_HEAVY         = "SFUIDisplay-Heavy"
    static let THEME_FONT_SF_DISPLAY_LIGHT         = "SFUIDisplay-Light"
    static let THEME_FONT_SF_DISPLAY_MEDIUM        = "SFUIDisplay-Medium"
    static let THEME_FONT_SF_DISPLAY_REGULA        = "SFUIDisplay-Regular"
    static let THEME_FONT_SF_DISPLAY_SEMIBOLD      = "SFUIDisplay-Semibold"
    static let THEME_FONT_SF_DISPLAY_THIN          = "SFUIDisplay-Thin"
    static let THEME_FONT_SF_DISPLAY_ULTRALIGHT    = "SFUIDisplay-Ultralight"
    
    static let THEME_FONT_SF_TEXT_BOLD             = "SFUIText-Bold"
    static let THEME_FONT_SF_TEXT_BOLDITALIC       = "SFUIText-BoldItalic"
    static let THEME_FONT_SF_TEXT_HEAVY            = "SFUIText-Heavy"
    static let THEME_FONT_SF_TEXT_HEAVYITALIC      = "SFUIText-HeavyItalic"
    static let THEME_FONT_SF_TEXT_ITALIC           = "SFUIText-Italic"
    static let THEME_FONT_SF_TEXT_LIGHT            = "SFUIText-Light"
    static let THEME_FONT_SF_TEXT_LIGHTITALIC      = "SFUIText-LightItalic"
    static let THEME_FONT_SF_TEXT_MEDIUM           = "SFUIText-Medium"
    static let THEME_FONT_SF_TEXT_MEDIUMITALIC     = "SFUIText-MediumItalic"
    static let THEME_FONT_SF_TEXT_REGULAR          = "SFUIText-Regular"
    static let THEME_FONT_SF_TEXT_SEMIBOLD         = "SFUIText-Semibold"
    static let THEME_FONT_SF_TEXT_SEMIBILDITALIC   = "SFUIText-SemiboldItalic"
    
}

class Constants {
    
    static let GOOGLE_MAP_KEY                    = "AIzaSyDVKSNmtiQru7lWdOcRapSx_t_a-DjnNUU"
    
    static let DEVICE_TOKEN_KEY                  = "DEVICE_TOKEN_KEY"
    
    static let DEFAULT_ERROR_MSG                 = "Unknown error"
    static let DEFAULT_SUCCESS_MSG               = "Success"
    static let DEFAULT_CODE_UNAUTHORIZED         = 401
    static let DEFAULT_ERROR_UNAUTHORIZED        = "Token Access Denied"
    
    static func generateFileNameByTimeStamp() -> String {
        return "\(String(Date().ticks)).png"
    }
}
extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

extension NSMutableAttributedString{
    func setColorForText(_ textToFind: String, with color: UIColor) {
        let range = self.mutableString.range(of: textToFind, options: .caseInsensitive)
        if range.location != NSNotFound {
            addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: range)
        }
    }
}
