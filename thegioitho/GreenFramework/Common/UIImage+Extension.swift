//
//  UIImage+Extension.swift
//  TheGioiTho
//
//  Created by HaiComet on 8/7/18.
//  Copyright © 2018 HaiComet. All rights reserved.
//

import UIKit

extension UIImage{
    
    static public func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    static public func image(image:UIImage, scaledToSize size:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if newImage == nil{
            return image
        }
        return newImage!
    }
    
    static public func image(image: UIImage, overwriteColor color:UIColor, widthAlpha alpha:CGFloat) -> UIImage{
        
        let screenScale = UIScreen.main.scale
        let newSize = CGSize(width: image.size.width, height: image.size.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, screenScale)
        let context = UIGraphicsGetCurrentContext()
        
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height), blendMode: .sourceOut, alpha: alpha)
        context?.setFillColor(color.cgColor)
        context?.setBlendMode(.sourceAtop)
        context?.setAlpha(alpha)
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        if resultImage != nil{
            return resultImage!
        }
        return image
    }
    
    static public func image(image: UIImage, overwriteColor color:UIColor) -> UIImage{
        return self.image(image: image, overwriteColor: color, widthAlpha: 1.0)
    }
    
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImageOrientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        } else {
            return self
        }
    }
}
