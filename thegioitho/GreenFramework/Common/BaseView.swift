//
//  BaseView.swift
//  TheGioiTho
//
//  Created by HaiComet on 8/7/18.
//  Copyright © 2018 HaiComet. All rights reserved.
//

import UIKit

class BaseView: UIView {
    
    @IBInspectable
    var radius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = radius
        }
    }
    

    
    @IBInspectable
    var borderLine: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderLine
        }
    }

  

    
}
