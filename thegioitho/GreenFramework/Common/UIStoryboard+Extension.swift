//
//  UIStoryboard+Extension.swift
//  TheGioiTho
//
//  Created by HaiComet on 8/7/18.
//  Copyright © 2018 HaiComet. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    enum Storyboard: String {
        case main = "Main"
        case company = "Company"
        case worker = "Worker"
    }
    
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    static func instantiateViewController<T: UIViewController>() -> T {
        let storyBoard = UIStoryboard(storyboard: .main)
        let vc:T = storyBoard.instantiateViewController(withIdentifier: String(describing: T.self)) as! T
        return vc
    }
    
    static func instantiateViewController<T: UIViewController>(storyboard: Storyboard) -> T {
        let storyBoard = UIStoryboard(storyboard: storyboard)
        let vc:T = storyBoard.instantiateViewController(withIdentifier: String(describing: T.self)) as! T
        return vc
    }
}
