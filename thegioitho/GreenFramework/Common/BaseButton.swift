//
//  BaseButton.swift
//  TheGioiTho
//
//  Created by HaiComet on 8/7/18.
//  Copyright © 2018 HaiComet. All rights reserved.
//

import UIKit

class BaseButton: UIButton {
    
    lazy private var aiLoading: UIActivityIndicatorView = {
        let aiLoading = UIActivityIndicatorView(activityIndicatorStyle: .white)
        aiLoading.hidesWhenStopped = true
        return aiLoading
    }()
    private var isAddLoading = false
    
    @IBInspectable
    var radius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = radius
        }
    }
    
    
    @IBInspectable
    var borderWith: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWith
        }
    }
    
   
    @IBInspectable
    var underline: Bool = false {
        didSet{
            if(underline){
                let forgotAttributes : [NSAttributedStringKey: Any] = [
                    NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : self.titleLabel?.font ?? UIFont.systemFont(ofSize: 15),
                    NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : self.titleLabel?.textColor ?? UIColor.black,
                    NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineStyle.rawValue) : NSUnderlineStyle.styleSingle.rawValue]
                let forgotString = NSMutableAttributedString(string: self.titleLabel?.text != nil ? (self.titleLabel?.text!)! : "", attributes: forgotAttributes)
                self.setAttributedTitle(forgotString, for: .normal)
            }
        }
    }
    
    override func setTitle(_ title: String?, for state: UIControlState) {
        
        super.setTitle(title, for: state)
        
        if(underline){
            let forgotAttributes : [NSAttributedStringKey: Any] = [
                NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : self.titleLabel?.font ?? UIFont.systemFont(ofSize: 15),
                NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : self.titleLabel?.textColor ?? UIColor.black,
                NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineStyle.rawValue) : NSUnderlineStyle.styleSingle.rawValue]
            let forgotString = NSMutableAttributedString(string: title != nil ? title! : "", attributes: forgotAttributes)
            self.setAttributedTitle(forgotString, for: .normal)
        }
    }
    
    func startLoading(){
        if isAddLoading == false{
            self.addSubview(aiLoading)
            aiLoading.snp.makeConstraints { (make) in
                make.right.equalTo(-10)
                make.width.equalTo(20)
                make.height.equalTo(20)
                make.centerY.equalToSuperview()
            }
            isAddLoading = true
        }
        aiLoading.startAnimating()
        self.parentViewController?.view.isUserInteractionEnabled = false
    }
    func stopLoading(){
        aiLoading.stopAnimating()
        if isAddLoading == true{
            aiLoading.removeFromSuperview()
            isAddLoading = false
        }
        self.parentViewController?.view.isUserInteractionEnabled = true
    }
    
    func enableBase(_ enable:Bool){
        if enable{
            self.isEnabled = true
            self.alpha = 1
        }
        else{
            self.isEnabled = false
            self.alpha = 0.6
        }
    }
    
    fileprivate var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

extension UIButton{
    var isBaseEnable:Bool{
        set(newValue){
            if newValue == true{
                //self.alpha = 1.0
                self.isEnabled = true
            }
            else{
                //self.alpha = 0.9
                self.isEnabled = false
            }
        }
        get{
            return self.isBaseEnable
        }
    }
}
