//
//  UIScrollView+Extension.swift
//  TheGioiTho
//
//  Created by HaiComet on 9/21/18.
//  Copyright © 2018 HaiComet. All rights reserved.
//

import UIKit

extension UIScrollView{
    func scrollToBotton(){
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: true)
    }
}
