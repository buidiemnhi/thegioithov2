//
//  MIDialog.swift
//  ReBook
//
//  Created by Lê Dũng on 9/18/17.
//  Copyright © 2017 Ledung. All rights reserved.
//

import UIKit

import Lottie
enum MIDialogType
{
    case info
    case warning
    case infoConfirm
    case warningConfirm
}



class MIDialog: GreenView {
    
    let successColor = "0086f6".hexColor()
    let buttonRadius = 4
    
    
    

    @IBOutlet weak var lbDes: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btCancel: UIButton!
    @IBOutlet weak var btAccept: UIButton!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var btClose: UIButton!
    var acceptBlock : (()->Void)!
    var cancelBlock : (()->Void)!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBAction func cancel(_ sender: Any) {
         self.removeFromSuperview()
        cancelBlock()
        remove()
       
            }
    @IBAction func closeTouch(_ sender: Any) {
         self.removeFromSuperview()
        cancelBlock()
        acceptBlock()
        remove()
        
    }
    
    func remove()
    {
        removeFromSuperview()
    }
    
    @IBAction func accept(_ sender: Any) {
         self.removeFromSuperview()
        acceptBlock()
        remove()
        
    }
    
    
    
    @IBOutlet weak var inimationView: UIView!
    func setSuccessImage()
    {
        addJsonAnim(name: "success")
    }
    
    func addJsonAnim(name : String)
    {
        let lotieView = LOTAnimationView(name:name) ;
        lotieView.contentMode = UIViewContentMode.scaleAspectFit
        inimationView.addSubview(lotieView)
        inimationView.setLayout(lotieView)

        lotieView.play()
        lotieView.loopAnimation = false
        
      //  lotieView.animationDuration = 2
        self.bringSubview(toFront: btClose)
        self.bringSubview(toFront: btAccept)
        self.bringSubview(toFront: btCancel)

    }
    
    func setWarningImage()
    {
       addJsonAnim(name: "warning")

    }
    
    
    func setBtAcceptTitle(title : String)
    {
        btAccept.setTitle(title, for: .normal)
        btClose.setTitle(title, for: .normal)
        
    }
    
    func setBtCancelTitle(title : String)
    {
        btCancel.setTitle(title, for: .normal)
        btClose.setTitle(title, for: .normal)
        
    }
    
    init(title : String,desc : String, type : MIDialogType, acceptBlock : @escaping (()->Void),cancelBlock : @escaping (()->Void))
    {
        super.init(frame: CGRect.init())
        self.acceptBlock = acceptBlock
        self.cancelBlock = cancelBlock
        
        alphaView.backgroundColor = UIColor.lightGray
        self.alphaView.alpha =  0.24
        lbTitle.text = title
        lbDes.text = desc
        btAccept.backgroundColor = successColor
        btAccept.setTitleColor(UIColor.white, for: .normal)
        btAccept.drawRadius(CGFloat(buttonRadius), color: UIColor.clear, thickness: 1)
        
        
        btClose.backgroundColor = successColor
        btClose.setTitleColor(UIColor.white, for: .normal)
        btClose.drawRadius(CGFloat(buttonRadius), color: UIColor.clear, thickness: 1)
        
        btCancel.backgroundColor = UIColor.white
        btCancel.setTitleColor(UIColor.black, for: .normal)
        btCancel.layer.borderColor = successColor.cgColor
        btCancel.drawRadius(CGFloat(buttonRadius), color: successColor, thickness: 1)
        
        
        
        
        switch type {
        case .info: infoStyle() ; break
        case .warning: warningStyle() ; break
        case .infoConfirm: infoConfirm() ; break
        case .warningConfirm: warningConfirm() ; break
            
        }
        
    }
    
    func infoConfirm()
    {
        btCancel.isHidden = false
        lbTitle.textColor = successColor
        headerView.backgroundColor = UIColor.white ;
        hsConfirm()
        setSuccessImage()
        
    }
    
    func warningConfirm()
    {
        lbTitle.textColor = UIColor.red
        headerView.backgroundColor = UIColor.white ;
        setWarningImage()
        hsConfirm()
        
    }
    
    func hsInfo()
    {
        btClose.isHidden = false
        btCancel.isHidden = true;
        btAccept.isHidden = true;
    }
    
    func hsConfirm()
    {
        btClose.isHidden = true
        btCancel.isHidden = false;
        btAccept.isHidden = false;
        
    }
    
    
    func infoStyle()
    {
        lbTitle.textColor = successColor
        headerView.backgroundColor = UIColor.white ;
        setSuccessImage()
        
        hsInfo()
    }
    
    func warningStyle()
    {
        lbTitle.textColor = UIColor.red
        headerView.backgroundColor = UIColor.white ;
        setWarningImage()
        
        hsInfo()
    }
    
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



extension NSObject
{
    
    
    func dialogInfoConfirm(_ title : String ,desc : String ,acceptTitle : String,cancelTitle : String,acceptBlock : @escaping (()->Void),cancelBlock : @escaping (()->Void))
    {
        dialog(title, desc: desc, acceptTitle: acceptTitle, cancelTitle: cancelTitle, type: .infoConfirm, acceptBlock: {
            acceptBlock()
        }) {
            cancelBlock()
        }
    }
    
    
    func dialogWarningConfirm(_ title : String ,desc : String ,acceptTitle : String,cancelTitle : String,acceptBlock : @escaping (()->Void),cancelBlock : @escaping (()->Void))
    {
        dialog(title, desc: desc, acceptTitle: acceptTitle, cancelTitle: cancelTitle, type: .warningConfirm, acceptBlock: {
            acceptBlock()
        }) {
            cancelBlock()
        }
    }
    
    
    func dialogWarning(_ title : String ,desc : String ,acceptTitle : String,acceptBlock : @escaping (()->Void))
    {
        dialog(title, desc: desc, acceptTitle: acceptTitle, cancelTitle: acceptTitle, type: .warning, acceptBlock: {
            
            acceptBlock()
            
        }) {
        }
    }
    
    
    func dialogInfo(_ title : String ,desc : String ,acceptTitle : String,acceptBlock : @escaping (()->Void))
    {
        dialog(title, desc: desc, acceptTitle: acceptTitle, cancelTitle: acceptTitle, type: .info, acceptBlock: {
            
            acceptBlock()
        }) {
        }
    }
    
    func dialog(_ title : String ,desc : String ,acceptTitle : String, cancelTitle : String, type : MIDialogType ,acceptBlock : @escaping (()->Void),cancelBlock : @escaping (()->Void))
    {
        let dialog = MIDialog.init(title: title, desc: desc, type: type, acceptBlock: { () in
            let appMiDialog = UIApplication.shared.delegate
            let subviews = appMiDialog?.window??.subviews
            
            subviews?.forEach{if($0 .isKind(of: MIDialog.self)){
                $0.removeFromSuperview()
                }}
            acceptBlock()
        
            
        }) { () in
            cancelBlock()
        }
        
        
        
        let appMiDialog = UIApplication.shared.delegate
        
        appMiDialog?.window??.addSubview(dialog)
        appMiDialog?.window??.setLayout(dialog)
        dialog.alpha = 0.0
        
        dialog.setBtCancelTitle(title: cancelTitle)
        dialog.setBtAcceptTitle(title: acceptTitle)
        
        
        dialog.contentView.drawRadius(6)
        UIView.animate(withDuration: 0.1) { () -> Void in
            dialog.alpha = 1.0
        }
        
        
    }
    
}

