//
//  UIViewController+Extension.swift
//  TheGioiTho
//
//  Created by HaiComet on 8/7/18.
//  Copyright © 2018 HaiComet. All rights reserved.
//

import UIKit

extension UIViewController{
    
    public typealias AlertDoneBlock = (UIAlertController?) -> Swift.Void
    public typealias AlertCancelBlock = (UIAlertController?) -> Swift.Void
    
    func navBarBackground(color:UIColor){
        
        let navSize = self.navigationController?.navigationBar.bounds.size
        let newSize = CGSize(width: navSize!.width, height: 64)//CGSize(width: navSize!.width, height: (navSize?.height)!+20)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.imageWithColor(color: color, size: newSize), for: .any, barMetrics: .default)
    }
    
    func navBarTitle(title: String){
        
        let label = UILabel()
        label.text = title
        label.textColor = UIColor.white
        label.font = UIFont(name: FontName.THEME_FONT_SF_TEXT_BOLD, size: 16)
        label.sizeToFit()
        self.navigationItem.titleView = label
    }
    
    func navBarImage(image:UIImage){
        let imageView = UIImageView(image: image)
        imageView.sizeToFit()
        self.navigationItem.titleView = imageView
    }
    
    func showAlert(title:String, message:String,doneTitle:String?,doneBlock:AlertDoneBlock?, cancelTitle:String? ,cancelBlock:AlertCancelBlock?){
        var onActionAlertDone: AlertDoneBlock?
        var onActionAlertCancel: AlertCancelBlock?
        
        onActionAlertDone = doneBlock
        onActionAlertCancel = cancelBlock
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if doneTitle != nil{
            let doneAction = UIAlertAction(title: doneTitle!, style: .cancel, handler: { (action) in
                if onActionAlertDone != nil{
                    onActionAlertDone!(alert)
                }
            })
            alert.addAction(doneAction)
        }
        if cancelTitle != nil{
            let cancelAction = UIAlertAction(title: cancelTitle!, style: .default, handler: { (action) in
                if onActionAlertCancel != nil{
                    onActionAlertCancel!(alert)
                }
            })
            alert.addAction(cancelAction)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
}
